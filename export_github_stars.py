#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import json
import datetime
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--username", default=None, help="a github username")
args = parser.parse_args()

if args.username:
    username = args.username
else:
    username = str(input("Github Username: "))

url = "https://api.github.com/users/{}/starred?page=".format(username)

headers = {'cookie': 'logged_in=no'}

count = 1

print("Exporting...")
filename = "github_stars_{0}_{1}.txt".format(username, datetime.datetime.now().strftime("%Y-%m-%d"))
file = open(filename, "w")

while count < 100:
    response = requests.request("GET", url + str(count), data="", headers=headers)
    response_json = response.json()

    for i in range(0, len(response_json)):
        file.write(response_json[i]["html_url"] + "\n")

    count += 1

    if len(response_json) <= 0:
        break

file.close()
print("Starred projects exported to " + filename)
